# Overview 

## Purpose:
 - Collect data about emerging issues from tool usage, (akin to a bug tracker)
 - Minimise amoun of data necessary to collect!

## Known Issues:
 - None as of July 2018. If any issues or suggestion please email: pilot-request@protective-h2020.eu
 - Jassim will get back to you ASAP.

## Todo:
 - Emailing capabilities to 'phone home'
 - New connectors will likely be added.

# Install instructions:

- Download and extract the whole logger.
- Double click index.html to open the page.

# Usage guidance:
- Keep updating with feedback as you use the system. We are hoping for anywhere between 3-5 logs per day. Documenting even small thoughts is useful. 
- At the end of the week, please send us the latest export file.

When you check protective and you have no comment, please add:
 - Comment type: `Neutral`
 - Topic: `System Check`
 - Module: `System Check`
and leave the other fields empty. That way we can record that the system is working as expected and you've recorded it for us.
If you have any issue or comment you'd like to add, please do.
